<?php

declare(strict_types=1);

use Slim\App;
use App\Http\Action;
use Slim\Routing\RouteCollectorProxy;

return static function (App $app): void {
    $app->get("/", Action\HomeAction::class);

    $app->group("/v1", function (RouteCollectorProxy $group): void {
        $group->group("/auth", function (RouteCollectorProxy $group) {
            $group->post("/join", Action\V1\Auh\Join\RequestAction::class);
            $group->post("/join/confirm", Action\V1\Auh\Join\ConfirmAction::class);
        });
    });
};
