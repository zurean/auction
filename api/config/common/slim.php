<?php

declare(strict_types=1);

use Slim\Interfaces\CallableResolverInterface;
use Psr\Container\ContainerInterface;
use Slim\CallableResolver;

return [
    CallableResolverInterface::class => static function (ContainerInterface $container): CallableResolverInterface {
        return new CallableResolver($container);
    }
];
