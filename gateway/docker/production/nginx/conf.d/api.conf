server {
    listen 80;
    server_name api.auction.zurean.ru;
    server_tokens off;

    include /etc/nginx/snippets/certbot.conf;

    rewrite ^(.*) https://api.auction.zurean.ru$1 permanent;
}

server {
    listen 443 ssl http2;
    server_name api.auction.zurean.ru;
    server_tokens off;

    ssl_certificate /etc/letsencrypt/live/api.auction.zurean.ru/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/api.auction.zurean.ru/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/api.auction.zurean.ru/chain.pem;

    include /etc/nginx/snippets/ssl.conf;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-NginX-Proxy true;
        proxy_pass        http://api;
        proxy_ssl_session_reuse off;
        proxy_redirect off;
    }
}
